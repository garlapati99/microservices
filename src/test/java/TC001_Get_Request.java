import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class TC001_Get_Request {
	
	
	@Test
	public void TC001_test_getWeatherDetails() {
		
		//Specify base URI
		RestAssured.baseURI="http://restapi.demoqa.com/utilities/weather/city";
		
		//Request object
		RequestSpecification httprequest = RestAssured.given();
		
		//Response object
		
		Response response = httprequest.request(Method.GET, "/Guntur");
		
		//Response verification
	
		System.out.println(response.asString());
		System.out.println(response.getBody().asString());
		
		//Status code validation
		
		int statusCode = response.getStatusCode();
		System.out.println(statusCode);
		Assert.assertEquals(statusCode, 200);
		
		//Status line verification
		
		String line = response.getStatusLine();
		System.out.println(line);
		
		Assert.assertEquals(line, "HTTP/1.1 200 OK");
		
	}

}
