import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TC002_Post_Request {
	
	@Test
	public void TC002_test_Post_Request() {
		
		RestAssured.baseURI="http://restapi.demoqa.com/customer";
		
		RequestSpecification postrequest = RestAssured.given();
		
		
		//Request post body parameters
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("FirstName", "Kalyan");
		requestParams.put("LastName", "Garlapati");
		requestParams.put("UserName", "garlapati99");
		requestParams.put("Password", "password");
		requestParams.put("Email", "garlapati99@gmail.com");
		
		postrequest.header("Content-Type", "application/json");
		
		
		postrequest.body(requestParams.toJSONString());
		
		Response response = postrequest.request(Method.POST, "/register");
		
		String output = response.getBody().asString();
		System.out.println(output);
		
		int statusCode = response.getStatusCode();
		System.out.println(statusCode);
	}

}
